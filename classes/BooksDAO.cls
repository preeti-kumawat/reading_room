public class BooksDAO {

    public static List<Book__c> insertBooks(List<Book__c> listOfBooks) {
        insert listOfBooks;
        return listOfBooks;
    }
    
    public static List<Book__c> updateBooks(List<Book__c> listOfBooks) {
        update listOfBooks;
        return listOfBooks;
    }
    
    public static void deleteBooks(List<Book__c> listOfBooks) {
        delete listOfBooks;
    }
}