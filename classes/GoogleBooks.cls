public class GoogleBooks {

    private static String url = 'https://www.googleapis.com/books/v1/volumes?q=';
    private static String key = 'AIzaSyDhXa-7ljOtYdmQZ--2keyl7oULuQ0l1XQ';
    private static Integer OFFSET_DEFAULT = 0;
    private static Integer COUNT_DEFAULT = 10;
    
    public static GoogleBooksWrapper getListByQuery(String query, Integer offset, Integer count) {
        offset = offset != null ? offset : OFFSET_DEFAULT;
        count = count != null ? count : COUNT_DEFAULT;
        String requestURL = url + query + '&key=' + key + '&startIndex=' + offset + '&maxResults=' + count;
        GoogleBooksWrapper listOfBooks = sendRequest(requestURL);
        return listOfBooks;
    }
    
    public static GoogleBooksWrapper sendRequest(String url) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(url);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            GoogleBooksWrapper results = (GoogleBooksWrapper) JSON.deserialize(response.getBody(), GoogleBooksWrapper.class);
            return results;
        }
        return null;
    }
    
    public class GoogleBooksWrapper {
        public String kind;
        public Integer totalItems;
        public List<GoogleBook> items;
    }
    
    public class GoogleBook {
        public String id;
        public VolumeInfo volumeInfo;
    }
    
    public class VolumeInfo {
        public String title;
        public String subtitle;
        public List<String> authors;
        public String publisher;
        public String description;
        public List<IndustryIdentifiers> industryIdentifiers;
        public Integer pageCount;
        public String language;
        public ImageLinks imageLinks;
        public Decimal averageRating;
    }
    
    public class IndustryIdentifiers {
        public String type;
        public String identifier;
    }
    
    public class ImageLinks {
        public String smallThumbnail;
        public String thumbnail;
    }
}