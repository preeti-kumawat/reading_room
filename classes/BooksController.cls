public with sharing class BooksController {

    @AuraEnabled 
    public static String searchListOfBooks(String query, Integer offset, Integer count) {
        GoogleBooks.GoogleBooksWrapper booksWrapper = GoogleBooks.getListByQuery(query, offset, count);
        return JSON.serialize(booksWrapper);
    }
}