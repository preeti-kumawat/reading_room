public with sharing class ReadingListController {
    
    @AuraEnabled
    public static List<Reading_List__c> getReadingList() {
        return ReadingListDAO.getReadingList();
    }
    
    @AuraEnabled
    public static List<Reading_List__c> getReadingListWithBooks() {
        return ReadingListDAO.getReadingListWithBooks();
    }
    
    @AuraEnabled
    public static List<Reading_List__c> insertReadingListAndAddBook(Reading_List__c newReadingList, String book) {
        GoogleBooks.GoogleBook bookInstance = (GoogleBooks.GoogleBook) JSON.deserialize(book, GoogleBooks.GoogleBook.class);
        Savepoint savepointInstance = Database.setSavepoint();
        try {
            ReadingListDAO.insertReadingList(new List<Reading_List__c>{ newReadingList });
            addBookToReadingList(newReadingList, book);
        } catch (Exception exceptionInstance) {
            Database.rollback(savepointInstance);
            throw new AuraHandledException('Sorry... Please try again.'); 
        }
        return getReadingList();
    }
    
    @AuraEnabled
    public static void addBookToReadingList(Reading_List__c readingListInstance, String book) {
        GoogleBooks.GoogleBook bookInstance = (GoogleBooks.GoogleBook) JSON.deserialize(book, GoogleBooks.GoogleBook.class);
        try {
            Book__c bookToInsert = new Book__c();
            bookToInsert.Name = bookInstance.volumeInfo.title;
            bookToInsert.Reading_List__c = readingListInstance.Id;
            bookToInsert.SpecialId__c = bookInstance.Id;
            BooksDAO.insertBooks(new List<Book__c>{bookToInsert});
        }
        catch (Exception exceptionInstance) {
            throw new AuraHandledException('Unable to add book. It seems like book is already added to any of the available lists.'); 
        }
    }
    
    @AuraEnabled
    public static void updateBook(String bookInstance) {
        try {
            Book__c book = (Book__c) JSON.deserialize(bookInstance, Book__c.class);
            BooksDAO.updateBooks(new List<Book__c> { book });
        }
        catch (Exception exceptionInstance) {
            throw new AuraHandledException('Unable to update book.'); 
        }
    }
    
    @AuraEnabled
    public static void deleteBook(String bookInstance) {
        try {
            Book__c book = (Book__c) JSON.deserialize(bookInstance, Book__c.class);
            BooksDAO.deleteBooks(new List<Book__c> { book });
        }
        catch (Exception exceptionInstance) {
            throw new AuraHandledException('Unable to delete book.'); 
        }
    }
}