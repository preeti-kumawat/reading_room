public class ReadingListDAO {

    public static List<Reading_List__c> getReadingList() {
        return [SELECT Id, Name FROM Reading_List__c ORDER BY Name];
    }
    
    public static List<Reading_List__c> getReadingListWithBooks() {
        return [SELECT Id, Name, 
                (SELECT Id, Name, Read__c FROM Books__r) 
                FROM Reading_List__c
                ORDER BY Name];
    }
    
    public static List<Reading_List__c> insertReadingList(List<Reading_List__c> readingList) {
        insert readingList;
        return readingList;
    }
}