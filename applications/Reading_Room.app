<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>Books_logo1</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>A library app</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Reading Room</label>
    <navType>Standard</navType>
    <tabs>Books</tabs>
    <tabs>Reading_List</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Reading_Room_UtilityBar</utilityBar>
</CustomApplication>
