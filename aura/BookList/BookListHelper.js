({
    getSetOfBooks : function(component, event, helper) {
        var action = component.get("c.getDefaultListOfBooks");
        action.setParams({
            offset : component.get("v.offset"),
            count : component.get("v.count")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.bookWrapper", JSON.parse(result));
            }
        });
        $A.enqueueAction(action);
	}
})