({
    addBookToReadingList : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var bookIndex = selectedItem.dataset.record;
        var selectedBook = component.get('v.bookWrapper.items')[bookIndex];
        component.set("v.selectedBook", selectedBook);
        component.set("v.isModalOpen", true);
    }
})