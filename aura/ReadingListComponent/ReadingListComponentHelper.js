({
    getReadingList : function(component, event, helper) {
        var action = component.get("c.getReadingListWithBooks");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var data = response.getReturnValue();
                var readingListWrapper = [];
                for(var i = 0; i < data.length; i++) {
                    readingListWrapper.push({'readingList' : data[i], 'isExpanded': false});
                }
                component.set("v.readingListWrapper", readingListWrapper);
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    updateBookStatus : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var selectedIndex = selectedItem.dataset.record;
        var readingListIndex = selectedIndex.split('#')[0];
        var bookIndex = selectedIndex.split('#')[1];
        var readingListWrapper = component.get("v.readingListWrapper");
        var readingListItem = readingListWrapper[readingListIndex];
        var book = readingListItem.readingList.Books__r[bookIndex];
        book.Read__c = true;
        readingListItem.readingList.Books__r[bookIndex] = book;
        readingListWrapper[readingListIndex] = readingListItem;
        var action = component.get("c.updateBook");
        action.setParams({
            'bookInstance' : JSON.stringify(book)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                component.set("v.readingListWrapper", readingListWrapper);
                helper.handleSuccess("Book '" + book.Name + "'has been mark as read");
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    deleteBook : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var selectedIndex = selectedItem.dataset.record;
        var readingListIndex = selectedIndex.split('#')[0];
        var bookIndex = selectedIndex.split('#')[1];
        var readingListWrapper = component.get("v.readingListWrapper");
        var readingListItem = readingListWrapper[readingListIndex];
        var book = readingListItem.readingList.Books__r[bookIndex];
        readingListItem.readingList.Books__r.splice(bookIndex);
        readingListWrapper[readingListIndex] = readingListItem;
        var action = component.get("c.deleteBook");
        action.setParams({
            'bookInstance' : JSON.stringify(book)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                component.set("v.readingListWrapper", readingListWrapper);
                helper.handleSuccess("Book '" + book.Name + "' has been deleted successfully.");
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    handleErrors : function(errors) {
        let toastParams = {
            title: "Error",
            message: "Unknown error",
            type: "error"
        };
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
    handleSuccess : function(message) {
        let toastParams = {
            title: "Success",
            message: message,
            type: "success"
        };
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
})