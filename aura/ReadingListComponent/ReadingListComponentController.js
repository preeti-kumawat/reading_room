({
    doInit : function(component, event, helper) {
        helper.getReadingList(component, event, helper);
    },
    toggleBooks : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var readingListIndex = selectedItem.dataset.record;
        var readingListWrapper = component.get("v.readingListWrapper");
        var readingListItem = readingListWrapper[readingListIndex];
        readingListItem.isExpanded = !readingListItem.isExpanded; 
        readingListWrapper[readingListIndex] = readingListItem;
        component.set("v.readingListWrapper", readingListWrapper);
    },
    markBookAsRead : function(component, event, helper) {
        helper.updateBookStatus(component, event, helper);
    },
    deleteSelectedBook : function(component, event, helper) {
        helper.deleteBook(component, event, helper);
    },
    showSpinner: function (component, event) {
        component.set("v.spinner", true); 
    },
    hideSpinner: function (component, event) {
        component.set("v.spinner", false); 
    }
})