({
    validateSearchWrapper : function(component, event, helper) {
        helper.resetError(component);
        var searchWrapper = component.get('v.searchWrapper');
        var queryString = '';
        var subQueries = [];
        if (searchWrapper.intitle) {
            subQueries.push('intitle:' + helper.trimAndReplaceSpaceWithPlus(searchWrapper.intitle));
        }
        if (searchWrapper.inauthor) {
            subQueries.push('inauthor:' + helper.trimAndReplaceSpaceWithPlus(searchWrapper.inauthor));
        }
        if (searchWrapper.isbn) {
            if(helper.trimAndReplaceSpaceWithPlus(searchWrapper.isbn) != searchWrapper.isbn) {
                helper.handleError(component, 'ISBN should not contain spaces');
                return false;
            }
            subQueries.push('isbn:' + searchWrapper.isbn);
        }
        if (subQueries.length == 0) {
            helper.handleError(component, 'Please fill atleast one field to search books');
            return false;
        }
        queryString = subQueries.join('+');
        component.set('v.queryString', queryString);
        return true;
    },
    trimAndReplaceSpaceWithPlus : function(text) {
        text = text.trim();
        return text.replace(/ +/g, '+');
    },
    handleError : function(component, errorMessage) {
        component.set('v.isError', true);
        component.set('v.errorMessage', errorMessage);
    },
    resetError : function(component) {
        component.set('v.isError', false);
        component.set('v.errorMessage', '');
    },
    searchBooks : function(component, event, helper) {
        var action = component.get("c.searchListOfBooks");
        action.setParams({
            query : component.get("v.queryString"),
            offset : component.get("v.offset"),
            count : component.get("v.count")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                var searchCompleteEvent = component.getEvent("bookSearchComplete");
                searchCompleteEvent.setParams({
                    bookWrapper: JSON.parse(result)
                }).fire();
            }
        });
        $A.enqueueAction(action);
    }
})