({
    clearSearch : function(component, event, helper) {
        var searchWrapper = {'inauthor' : '', 'intitle' : '', 'isbn' : ''};
        component.set("v.searchWrapper", searchWrapper);
        helper.resetError(component);
    },
    searchBooks : function(component, event, helper) {
        var valid = false;
        valid = helper.validateSearchWrapper(component, event, helper);
        if (valid) {
            helper.searchBooks(component, event, helper);
        }
    },
})