({
    getReadingList : function(component, event, helper) {
        var action = component.get("c.getReadingList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.readingList", result);
            }
        });
        $A.enqueueAction(action);
    },
    insertReadingList : function(component, event, helper) {
        var newReadingList = component.get("v.newReadingList");
        var book = component.get("v.book");
        var action = component.get("c.insertReadingListAndAddBook");
        action.setParams({
            'newReadingList' : newReadingList,
            'book' : JSON.stringify(book)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.readingList", result);
                helper.reset(component, event, helper);
                helper.handleSuccess(component, event, helper, newReadingList);
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    reset : function(component, event, helper) {
        component.set('v.isCreate', false);
        component.set('v.isView', true);
        component.set("v.isModalOpen", false);
        component.set("v.newReadingList", "{'sobjectType':'Reading_List__c', 'Name':''}");
    },
    addBookToReadingList : function(component, event, helper, readingListInstance) {
        var book = component.get("v.book");
        var action = component.get("c.addBookToReadingList");
        action.setParams({
            'readingListInstance' : readingListInstance,
            'book' : JSON.stringify(book)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                helper.reset(component, event, helper);
                helper.handleSuccess(component, event, helper, readingListInstance);
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    handleErrors : function(errors) {
        let toastParams = {
            title: "Error",
            message: "Unknown error",
            type: "error"
        };
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
    handleSuccess : function(component, event, helper, readingListInstance) {
        var book = component.get("v.book");
        let toastParams = {
            title: "Success",
            message: "Successfully Operated",
            type: "success"
        };
        if (readingListInstance) {
            toastParams.message = "Book '" + book.volumeInfo.title + "' has been added to '" + readingListInstance.Name + "' list.";
        }
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
})