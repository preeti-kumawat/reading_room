({
    closeModel : function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    doInit : function(component, event, helper) {
        helper.getReadingList(component, event, helper);
    },
    addReadingList : function(component, event, helper) {
        component.set('v.isCreate', true);
        component.set('v.isView', false);
    },
    cancel : function(component, event, helper){
        component.set('v.isCreate', false);
        component.set('v.isView', true);
    },
    createNewReadingList : function(component, event, helper) {
        var newReadingList = component.get("v.newReadingList");
        if (newReadingList !=null && newReadingList.Name != null) {
            helper.insertReadingList(component, event, helper);
        }
    },
    addBookToSelectedReadingList : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var listIndex = selectedItem.dataset.record;
        var readingListInstance = component.get('v.readingList')[listIndex];
        helper.addBookToReadingList(component, event, helper, readingListInstance);
    }
})