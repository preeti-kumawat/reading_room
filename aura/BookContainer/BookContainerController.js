({
    showSpinner: function (component, event) {
        component.set("v.spinner", true); 
    },
    hideSpinner: function (component, event) {
        component.set("v.spinner", false); 
    },
    handleBookSearchComplete : function (component, event, helper) {
        component.set('v.bookWrapper', event.getParam('bookWrapper'));
    }
})